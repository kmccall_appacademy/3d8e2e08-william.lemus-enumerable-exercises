require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, el| acc + el }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |str| string_in_string(str, substring) }
end

def string_in_string(long_string, string)
  long_string.include?(string)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  arr = string.split.join.split("")
  arr.select { |ch| arr.count(ch) > 1}.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted = string.split.sort_by { |word| clean_punctuation(word).length }
  sorted.reverse[0..1]
end

def clean_punctuation(word)
  letters = ("a".."z")
  word.chars.select { |ch| letters.include?(ch.downcase) }.join
end
# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = ("a".."z").to_a
  letters.reject { |ch| string.include?(ch) }


end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = []
  (first_yr..last_yr).each do |year|
    years << year if not_repeat_year?(year)
  end
  years
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  # check no repeats on that one song
  result = []
  songs.each{|song| result << song if no_repeats?(song, songs)}
  # add it to result if it has no repeat_years
  # return result
  result.uniq
end

def no_repeats?(song_name, songs)
  # find if song appears twice in a row
  (1...songs.length).each do |idx|
    return false if song_name == songs[idx-1] && songs[idx] == song_name
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  arr = string.split
  # clean punctuation
  arr.map! { |word| clean_punctuation(word) }
  closest_idx = -1
  closest_distance = -1
  arr.each_with_index do |el, idx|
    dist = c_distance(el)
    if new_min_distance(dist, closest_distance)
      closest_idx = idx
      closest_distance = dist
    end
  end
  return arr[closest_idx] if closest_idx != -1
  ""
end

def c_distance(word)
  distance = word.reverse.index("c")
  return distance unless distance.nil?
  -1
end

def new_min_distance(dist, cur_dist)
  dist < cur_dist && dist != -1 || cur_dist == -1
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  # select an array value
  num = arr[0]
  range_start = 0
  result = []
  # check to see if next one is part of ranges
  (1...arr.length).each do |idx|
    # keep going until there is a new number
    next if num == arr[idx]
    # see if its a valid range
    result.push([range_start,idx- 1]) if range_start < idx - 1
    range_start = idx
    num = arr[idx]
  end
  # add the last element if it also is a range.
  result.push([range_start, arr.length - 1]) if range_start < arr.length - 1
  result


end
